import base64
from PIL import Image
from io import BytesIO
import json
from json_encoder import json_encoder
import torch
from torchvision import datasets,transforms
from torch.utils.data import DataLoader,TensorDataset,Dataset
import os 
import time
path='/home/andres/facedetection_demo/imagetest'
objeto=json_encoder(path)
file_list=os.listdir(path)

for f in file_list:
	label=1
	coded=objeto.encode(f,label,label)
	
objeto.json_save('mnti.json')



with open("mnti.json", "rb") as file:
	res=json.load(file)



n=len(res)
mean = [0.4979,0.4532,0.4453]
std = [0.22495,0.22385,0.2213]
size=32
transformadas=transforms.Compose([transforms.Resize((size,size)),transforms.ToTensor(),transforms.Normalize(mean=mean,std=std)])
tensor_data=torch.zeros((n,3,32,32))
tensor_labels=torch.zeros(n)




i=0
for name in res:
	img_result=objeto.decode(res[name]['image'])
	label_1=res[name]['label']

	tensor_img=transformadas(img_result)
	tensor_data[i,:,:,:]=tensor_img
	tensor_labels[i]=label_1
	i+=1
dataset=TensorDataset(tensor_data,tensor_labels)
dataloader=DataLoader(dataset,batch_size=32)
for x,y in dataloader:
	print(x)





'''
with open("benito.jpg", "rb") as image_file:
    data = base64.b64encode(image_file.read())

imgs_dict={}
imgs_dict["benito.jpg"]=data
print(imgs_dict)
json_file=json.dumps(imgs_dict)
#with open("train.json","w+") as out:
#	json.dump(imgs_dict,out)


im = Image.open(BytesIO(base64.b64decode(data)))
im.save('benito_decode.jpg')
'''