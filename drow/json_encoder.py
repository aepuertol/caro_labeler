import json
import base64
from PIL import Image
from io import BytesIO


class json_encoder():
	def __init__(self,folder_path):
		self.global_dict={}
		self.general_path=folder_path
	def encode(self,image_path,label_eyes,label_yawn):
		with open(self.general_path+"/"+image_path,"rb") as file:
			img=file.read()
			self.img_coded=base64.encodebytes(img).decode('utf-8')
			self.global_dict[image_path]={}
			self.global_dict[image_path]['image']=self.img_coded
			self.global_dict[image_path]['eyes']=label_eyes
			self.global_dict[image_path]['yawn']=label_yawn
			return self.img_coded
	def json_save(self,file_name):
		with open(file_name,"w+") as file:
			json.dump(self.global_dict,file)
	def decode(self,coded):
		img = Image.open(BytesIO(base64.b64decode(coded)))
		return img



	




	





