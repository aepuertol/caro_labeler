import os

import dash
import dash_core_components as dcc
import dash_html_components as html
#from json_encoder import json_encoder
import json


external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

server = app.server
filename='drozy.json'
with open(filename,'rb') as file:
    data=json.load(file)
names=[i for i in data]
print(len(data))
#json_handler=json_encoder('')
#decoded=json_handler.decode(data[names[0]]['image'])
#print(decoded)
cuenta=0

app.layout = html.Div([
    
    html.Div(id='display-image'),
    html.Div(id='name'),
    html.Div(id='convention',children=['Convención Abierto : 0 Cerrado: 1'],style={'font-weight':'bold'}),
    html.Div(id='eyes',children=[html.P('Eyes'),dcc.Dropdown(id='eye_status', options=[{'label':'0','value':'0'},{'label':'1','value':'1'}],style={'width':'50%'})]),
    html.Div(id='yawn',children=[html.P('Mouth'),dcc.Dropdown(id='yawn_status', options=[{'label':'0','value':'0'},{'label':'1','value':'1'}],style={'width':'50%'})]),
    
    html.Button('⬅️', id='backward', n_clicks=0),
    html.Button('➡️', id='submit-val', n_clicks=0),
    html.Button('💾', id='save-val', n_clicks=0),
    html.Div(id='cuenta'),
    html.Div(id='actual'),
    html.Div(id='saved'),
    html.Div(id='num',style={'display':'none'}),
    
    

])

@app.callback(dash.dependencies.Output('num', 'children'),
              
             [dash.dependencies.Input('submit-val', 'n_clicks'),
              dash.dependencies.Input('backward', 'n_clicks'),])
def move(n1,n2):
	event=dash.callback_context.triggered[0]['prop_id']
	with open('cuenta.json','rb') as file:
            r=json.load(file)
	if n1>0 and 'submit-val' in event :
		r['cuenta']+=1
	if n2>0 and 'backward' in event:
		r['cuenta']-=1
	with open('cuenta.json','w+') as file:
                print(r)
                json.dump(r,file)
	
		
	return f"{r['cuenta']}"
	

@app.callback(dash.dependencies.Output('display-image', 'children'),
              dash.dependencies.Output('eye_status', 'value'),
              dash.dependencies.Output('yawn_status', 'value'),
              dash.dependencies.Output('cuenta', 'children'),
              dash.dependencies.Output('actual', 'children'),
              dash.dependencies.Output('name', 'children'),
              [dash.dependencies.Input('num', 'children')])
def display_value(n):
    #if n_clicks<len(data) :
        with open('cuenta.json','rb') as file:
            r=json.load(file)
            
        print(r)
        n=int(n)
        '''        
        if n_clicks>0:
            r['cuenta']+=1
            with open('cuenta.json','w+') as file:
                print(r)
                json.dump(r,file)
        '''
        #n=r['cuenta']
        out=html.Img(src='data:image/jpg;base64,{}'.format(data[names[n]]['image']))
        outeyes,outyawn=data[names[n]]['eyes'],data[names[n]]['yawn']
        
        aeyes,ayawn=data[names[n]]['eyes'],data[names[n]]['yawn']
        actual=f"Ojos:{aeyes},Boca:{ayawn}"
        
        
        



        cuenta=f'{n+1}/{len(data)}'
        #with open('mi_oficina_v1_backup_final.json',"w+") as file2:
            #json.dump(data,file2)
        #retornar el numero en que va para poder guardar
        return out,outeyes,outyawn,cuenta,'status guardado: '+actual,names[n]
@app.callback(dash.dependencies.Output('saved','children'),

              [dash.dependencies.Input('save-val','n_clicks'),
              dash.dependencies.State('eye_status','value'),
              dash.dependencies.State('yawn_status','value'),
              dash.dependencies.State('name','children')])
def save_status(n_clicks,eye,yawn,name):
    if n_clicks>0:
        old_eyes=data[name]['eyes']
        old_yawn=data[name]['yawn']
        data[name]['eyes']=eye
        data[name]['yawn']=yawn
        with open(filename,'w+') as file:
            json.dump(data,file)



        return f'Ultima modificación: {name} Se actualizó Ojos: {old_eyes} Bostezo:{old_yawn} por Ojos: {eye} Bostezo: {yawn}'
    else:
        return ''

if __name__ == '__main__':
    app.run_server(debug=True)
