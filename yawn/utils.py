import plotly.express as px
import plotly.graph_objects as go
from icecream import ic
from PIL import Image
import sqlite3
import pandas as pd
class imager:
	
	def __init__(self,dbpath):
		self.conn=sqlite3.connect(dbpath,check_same_thread=False)
		self.cur=self.conn.cursor()
		
		
	def get_quant(self):
		self.df=pd.read_sql("""SELECT * FROM cuenta""",self.conn)
		return int(self.df.loc[0,'quant'])
		
	def show_image(self,img_name):					
		fig=px.imshow(Image.open(img_name))
		fig.update_layout(height=400,width=400)		
		return fig
		
	def get_name(self,idx):
		self.df=pd.read_sql("""SELECT * FROM images""",self.conn)
		return self.df.loc[idx,'name'],self.df.loc[idx,'class']
		
	def advance(self,actual_idx):
		#actual_idx=self.get_quant()
		query=f"""UPDATE cuenta SET quant='{actual_idx+1}' """
		print(query)
		self.cur.execute(query)
		self.conn.commit()
	def reverse(self,actual_idx):
		#actual_idx=self.get_quant()
		query=f"""UPDATE cuenta SET quant='{actual_idx-1}' """
		self.cur.execute(query)
		self.conn.commit()
	def update_class(self,spec_img_name,class_name):
		query=f"""UPDATE images SET class='{class_name}' WHERE name='{spec_img_name}'"""
		print(query)
		self.cur.execute(query)
		self.conn.commit()
	def get_total_data(self):
		self.df=pd.read_sql("""SELECT COUNT(*) as total FROM images""",self.conn)
		return int(self.df.loc[0,'total'])
	def go_to(self,n):
		query=f"""UPDATE cuenta SET quant='{n}' """
		self.cur.execute(query)
		self.conn.commit()
	def get_null(self):
		self.df_complete=pd.read_sql("""SELECT *  FROM images """,self.conn)
		self.df_null=self.df_complete[self.df_complete['class'].isnull()]
		return self.df_null
		
		
		
		
		
		
		
	
	
	
		
		
		
		
