from dash import Dash, dcc, html, Input, Output,ctx,State

import os
import sqlite3
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
from icecream import ic
from PIL import Image
from utils import imager


external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = Dash(__name__, external_stylesheets=external_stylesheets)

server = app.server

db_object=imager('yawndb.db')
total=db_object.get_total_data()
ic(total)

app.layout = html.Div([
    html.H2('Phototagger 🐸 🐵'),
    html.Div(id='alerts',style={'margin-left':'40%','color':'red'}),
    
    html.Div(id='photo-div',children=[dcc.Graph(id='img')],style={'margin-left':'30%'}),
    html.Div(id='warnings',style={'margin-left':'40%','font-size':'20px'}),
    html.Br(),
    
    html.Div([
    	html.Button('sonrisa',id='smile-btn',n_clicks=0,style={'display':'inline-block'}),
    	html.Button('normal',id='normal-btn',n_clicks=0,style={'display':'inline-block'}),
    	html.Button('bostezo',id='yawn-btn',n_clicks=0,style={'display':'inline-block'}),
   	 html.Button('ignorar',id='ignore-btn',n_clicks=0,style={'display':'inline-block'})
    ],style={'margin-left':'30%'}),
    html.Br(),
    html.Div([
    	
    	html.Button('retroceder',id='back-btn',n_clicks=0,style={'display':'inline-block'}),
    	html.Button('avanzar',id='next-btn',n_clicks=0,style={'display':'inline-block'})
    ],style={'margin-left':'40%'}),
    html.Br(),
    html.Div([
    	dcc.Input(id='image-number',type='text',value=db_object.get_quant()+1),
    	html.Button('Ir',id='go-btn')
    
    ],style={'margin-left':'40%'}),
    html.Div([
    	dcc.Checklist(id='see-remain',options=[{'label': 'Ver imágenes faltantes', 'value': 'remain'}])

    
    ],style={'margin-left':'40%'}),
    
    html.Div(id='remain_div',style={'overflow':'auto'})
    
    
])


@app.callback(Output('img','figure'),
              Output('warnings','children'),
              Output('alerts','children'),
              Output('remain_div','children'),
				  [Input('smile-btn','n_clicks'),
				  Input('normal-btn','n_clicks'),
				  Input('yawn-btn','n_clicks'),
				  Input('ignore-btn','n_clicks'),
				  Input('next-btn','n_clicks'),
				  Input('back-btn','n_clicks'),
				  Input('go-btn','n_clicks'),
				  Input('see-remain','value'),
				  State('image-number','value') ])
def tagger(sonrisa,normal,bostezo,ignore,advance,back,gobtn,val,imgn):
	fig=go.Figure()
	w=''
	alert=''
	msg=''
	try:
		
		actual_idx=db_object.get_quant()
		spec_name,classn=db_object.get_name(actual_idx)
		w=f'Archivo: {spec_name}  Clase: {classn}'
		fig=db_object.show_image(spec_name)
		ic(spec_name)
		ic('inicio',actual_idx)
		alert=f'{actual_idx+1}/{total}'
		
	
	
		if "smile-btn"==ctx.triggered_id:
			
			db_object.update_class(spec_name,'smile')
			w=f'Archivo: {spec_name}  Clase: smile'
			
			ic('smile')		
		
			
		elif "normal-btn"==ctx.triggered_id:
			db_object.update_class(spec_name,'normal')
			w=f'archivo: {spec_name}  clase: normal'
						
			ic('normal')		
			
		
		elif "yawn-btn"==ctx.triggered_id:
			
			db_object.update_class(spec_name,'yawn')
			w=f'Archivo: {spec_name}  Clase: yawn'
								
			ic('yawn')				
			
		
		elif "ignore-btn"==ctx.triggered_id:
			db_object.update_class(spec_name,'ignore')
			w=f'Archivo: {spec_name}  Clase: ignore'
			
						
			ic('ignore')
		elif "back-btn"==ctx.triggered_id:
			actual_idx=db_object.get_quant()
			ic('actual',actual_idx)
			if actual_idx<=0:
				alert=f'{actual_idx+1}/{total}'
			else:
				db_object.reverse(actual_idx)
				actual_idx=db_object.get_quant()
				ic('nuevo',actual_idx)
				spec_name,classn=db_object.get_name(actual_idx)
				fig=db_object.show_image(spec_name)
				w=f'Archivo: {spec_name}  Clase: {classn}'
				alert=f'{actual_idx+1}/{total}'
		elif "next-btn"==ctx.triggered_id:
			
			actual_idx=db_object.get_quant()
			ic('actual',actual_idx,total)
				
			if actual_idx<total-1:
				ic('avanza')
				db_object.advance(actual_idx)
				actual_idx=db_object.get_quant()
				spec_name,classn=db_object.get_name(actual_idx)
				fig=db_object.show_image(spec_name)
				ic('nuevo',actual_idx)
				w=f'Archivo: {spec_name}  Clase: {classn}'
				alert=f'{actual_idx+1}/{total}'
			else:
				alert=f'{actual_idx+1}/{total}'
		elif "go-btn"==ctx.triggered_id:
			ic(gobtn,imgn)
			if int(imgn)<=total and int(imgn)>=1 :
				db_object.go_to(int(imgn)-1)
				actual_idx=db_object.get_quant()
				spec_name,classn=db_object.get_name(actual_idx)
				fig=db_object.show_image(spec_name)
				ic('nuevo',actual_idx)
				w=f'Archivo: {spec_name}  Clase: {classn}'
				alert=f'{actual_idx+1}/{total}'
		if val is None:
			val=[]
		
		if len(val)>0:
			df=db_object.get_null()
			if len(df)>0:
				msg="ID imágenes: "
				msg+=" ; ".join([str(i+1) for i in df.index])
			else:
				msg='No hay imágenes faltantes'
		
			
		
		
				
			
	
	except Exception as e:
		
		fig=go.Figure()
		ic(e)
		w='Error'
		
		
	return fig,w,alert,msg
"""
@app.callback(Output('remain_div','children'),
             [Input('see-remain','value')])
def show_remain(val):
	if val is None:
		val=[]
	msg=''
	if len(val)>0:
		df=db_object.get_null()
		print(df)
		msg=" ; ".join([str(i+1) for i in df.index])
		
	return msg
"""
	
	
	




if __name__ == '__main__':
    app.run_server(debug=True)